[[_TOC_]]

# Exercice 1

## Question 1

On compile avec _arm-none-eabi-gcc -Os -S t.c_ le code suivant :

```c
#include <stdint.h>

uint32_t a; // global variable

__attribute__((naked)) void f() {
    for (uint8_t i=0; i<=a; i++)
        g();
}
```

On obtient alors :
```nasm
f:
    mov r4, #0
    ldr r5, .L4
.L2:
    bl  g
    add r4, r4, #1
    ldr r3, [r5]
    and r4, r4, #255
    cmp r4, r3
    bls .L2
```
Où L4 a vocation a être remplacé par l'adresse de la variable globale a lors de l'édition des liens.

## Question 2

Si maintenant i est un unsigned int on obtient :

```nasm
f:
    mov r4, #0
    ldr r5, .L4
.L2:
    bl  g
    ldr r3, [r5]
    add r4, r4, #1
    cmp r3, r4
    bcs .L2
```
L'instruction and qui correspondait au masquage du registre 32bits pour conserver la valeur modulo 255 comme doit le faire un uint8_t a disparue, en effet la variable i fait maintenant 32 bits le registre a donc le même comportement qu'elle.

L'instruction bls (branch lower or same) a été remplacée par l'instruction bcs (branch if carry is set) l'instruction cmp effectuant l'opération (a - i) le flag carry est à 1 en cas d'overflow sur un unsigned int c'est à dire ici dans le cas où i <= a (penser qu'une soustraction est une addition avec un complément à 1). On peut supposer que cette instruction est plus simple à traiter pour le processeur et donc que gcc l'utilise dans ce genre de cas.


# Exercice 2

## Question 1

```c
// Global variables
uint32_t *a;
uint32_t *b;
uint32_t *c;
...
*a += *c;
*b += *c;
```

Une traduction mot à mot de ce code c en assembleur donnerait :

```nasm
ldr r0, .a
ldr r1, .b
ldr r2, .c
ldr r3, [r0]
ldr r4, [r1]
ldr r5, [r2]
add r3, r3, r5
str r3, [r0]
add r4, r4, r5
str r4, [r1]
```

## Question 2

La compilation (`arm-none-eabi-gcc -Os -S var.c`) donne :
```nasm
f:
    ldr r3, .L2
    ldm r3, {r0, r1}
    ldr ip, [r1]
    ldr r2, [r0]
    add r2, r2, ip
    str r2, [r0]
    ldr r2, [r3, #8]
    ldr r1, [r1]
    ldr r3, [r2]
    add r3, r3, r1
    str r3, [r2]
```

- Le compilateur utilise l'instruction load multiple pour charger les ints juxtaposés en mémoire à partir de l'adresse stockée dans r3 dans les registres r0 et r1.

*À partir d'ici r0 et r1 contiennent respectivement les adresses de a et de c.*

- La valeur pointée par r1 est chargée dans ip (=r12) et celle pointée par r0 dans r2, leur somme est stocké dans r2 puis écrite en mémoire à l'adresse pointée par r0.

*On a donc effectué \*a += \*c*

- ldr r2, [r3, #8] charge l'adresse de la variable b dans r2. Les deux lignes suivantes mettent respectivement la valeur de c dans r1 et la valeur de b dans r3.

- On effectue l'addition et on stocke le résultat à l'adresse pointée par r2 (c'est à dire b).

**On remarque que pour chaque calcul c est rechargé de la mémoire plutôt que d'être conservé dans un registre en effet rien ne permet au compilateur de savoir si b et c ne pointent pas sur la même adresse auquelle cas après le premier calcul la valeur pointée par c aurait changée.
Lorsque les pointeurs sont passés en paramètres d'une fonction en C on peut éviter ce genre d'optimisation du compilateur en utilisant le mot-clé restrict.**

# Exercice 3

## Question 1

```c
#include <string.h> // strdup
#include <stdio.h> // printf
#include <stdlib.h> // free

char str1[] = "tambour"; // data
const char str2[] = "salopette"; // rodata
char str3[10]; // bss

int main(void){
    char str4[] = "badaboom"; // stack
    char* str5 = strdup(str4); // heap


    printf("text : %p\n", main);
    printf("rodata : %p\n", str2);
    printf("data : %p\n", str1);
    printf("bss : %p\n", str3);
    printf("heap : %p\n", str5);
    printf("stack : %p\n", str4);

    free(str5);

    return 0;
}
```


Résultat
```
text : 0x401146
rodata : 0x402010
data : 0x404038
bss : 0x404048
heap : 0x19202a0
stack : 0x7ffe52b1f39f
```

On constate que les segments sont stockés dans l'ordre .text > .rodata > .data > .bss > heap > stack

## Question 2

```c
#include <stdio.h>

int fibo(int n) {
    printf("n = %d, &n = %p\n", n, &n);
    return (n <= 1) ? n : fibo(n-1) + fibo(n-2);
}

int main(int argc, char* argv) {
    fibo(5);
    return 0;
}
```

Résultat
```
n = 5, &n = 0x7ffe170bebac
n = 4, &n = 0x7ffe170beb7c
n = 3, &n = 0x7ffe170beb4c
n = 2, &n = 0x7ffe170beb1c
n = 1, &n = 0x7ffe170beaec
n = 0, &n = 0x7ffe170beaec
n = 1, &n = 0x7ffe170beb1c
n = 2, &n = 0x7ffe170beb4c
n = 1, &n = 0x7ffe170beb1c
n = 0, &n = 0x7ffe170beb1c
n = 3, &n = 0x7ffe170beb7c
n = 2, &n = 0x7ffe170beb4c
n = 1, &n = 0x7ffe170beb1c
n = 0, &n = 0x7ffe170beb1c
n = 1, &n = 0x7ffe170beb4c
```

On constate que les variable sont stackées de plus en plus bas dans la pile au fur et à mesure que la taille de la pile augmente.

# Exercices 4

On étudie le code suivant :

```c
#include <stdint.h>
#include <stdio.h>

int32_t x = 34;
int32_t y;
const char mesg[] = "Hello World!\n";

int main() {
    static uint8_t z;
    uint16_t t;

    y = 12;
    z = z + 1;
    t = y+z;

    printf(mesg);
    printf("x = %d, y = %d, z = %d, t = %d\n",
           x, y, z, t);
    return 0;
}
```

## Question 1

### Optimisation -O0

Avec la commande `arm-none-eabi-objdump -h sections0 |cut -c -28|sed -n 'n;p'` on obtient les tailles des différentes sections :

```
Sections:
Idx Name          Size    
  0 .text         000000b8  
  1 .data         00000004  
  2 .bss          00000005  
  3 .rodata       00000040  
  4 .comment      0000004a  
  5 .ARM.attributes 0000002a
```

En examinant la section _.rodata_ (`arm-none-eabi-objdump -s sections0 -j .rodata`) on s'aperçoit qu'elle contient la chaîne "Hello World" terminée par un saut de ligne ainsi que la même chaîne nue.

```
Contents of section .rodata:
 0000 48656c6c 6f20576f 726c6421 0a000000  Hello World!....
 0010 48656c6c 6f20576f 726c6421 00000000  Hello World!....
 0020 78203d20 25642c20 79203d20 25642c20  x = %d, y = %d, 
 0030 7a203d20 25642c20 74203d20 25640a00  z = %d, t = %d..
```

En désassemblant le code on trouve :

```nasm
Disassembly of section .text:

00000000 <main>:
...
  58:   e59f004c    ldr r0, [pc, #76]   ; ac <main+0xac>
  5c:   ebfffffe    bl  0 <puts>
...
  ac:   00000010    .word   0x00000010
...
```
Là où on s'attendrait à voir appelé printf c'est en fait puts qui est appelé.

De plus en regardant dans _.rodata_ l'adresse 0x00000010 que l'on passe à puts est celle de la chaîne de caractère sans retour à la ligne.

Et en effet le man de puts indique _"puts() écrit la chaîne de caractères s et un retour chariot final sur stdout."_ : l'utilisation de cette fonction par le compilateur permet donc de s'affranchir du passage du retour à la ligne. gcc préfère utiliser puts à printf lorque cela est possible (chaîne simple terminée par un retour à la ligne), et ce même sans avec le niveau d'optimisation minimum, car cette dernière est plus lourde en raison du traitement de la chaîne formatée.


### Optimisation -O1

Examinons à nouveau la taille de chaque section
```
Sections:
Idx Name          Size    
  0 .text         0000006c  
  1 .data         00000004  
  2 .bss          00000005  
  3 .rodata.str1.4 00000030 
  4 .rodata       0000000e  
  5 .comment      0000004a  
  6 .ARM.attributes 0000002a
```

On remarque en premier que la section .text fait 0xb8 - 0x6c = 0x4c caractère de moins qu'avec l'option -O0. En effet sans optimisation le main est composé de 41 instructions machines contre seulement 25 avec le premier niveau d'optimisation.

En examinant le contenu des sections (`arm-none-eabi-objdump -s sections1`) on s'aperçoit que cette fois-ci seule la chaine terminée par un saut de ligne est dans _.rodata_, les autres sont regroupées dans _.rodata.str1.4_
```
Contents of section .rodata.str1.4:
 0000 48656c6c 6f20576f 726c6421 00000000  Hello World!....
 0010 78203d20 25642c20 79203d20 25642c20  x = %d, y = %d, 
 0020 7a203d20 25642c20 74203d20 25640a00  z = %d, t = %d..
Contents of section .rodata:
 0000 48656c6c 6f20576f 726c6421 0a00      Hello World!..
```

On peut déduire des observations que la section _.rodata.str1_ contient les chaines de caractères uniquement destinés à être affichés.

Si maintenant on effectue l'édition des liens en fournissant le fichier objet bidon.o compilé à partir du bidon.c ci-dessous :
```c
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

void _exit(int a) {while(1);}
void *_sbrk(int a) {}
int _write(int fd, const void *buf, size_t count) {}
int _close(int fd) {}
int _fstat(int fd, struct stat *buf) {}
int _isatty(int fd) {}
off_t _lseek(int fd, off_t offset, int whence) {}
ssize_t _read(int fd, void *buf, size_t count) {}
int _kill(pid_t pid, int sig) {}
pid_t _getpid(void) {}
```

Alors en examinant le début de .rodata (`arm-none-eabi-objdump -s -j _.rodata_ sections1.elf | head -n 11`) on retrouve toutes nos chaines de caractères qu'elles viennent de _.rodata_ ou de _.rodata.str1.4_

```
sections1.elf:     file format elf32-littlearm

Contents of section .rodata:
 13f10 3b3e0b3e 103e253e 333e033e 063e143e  ;>.>.>%>3>.>.>.>
 13f20 0e3e0d3e 243e123e 273e023e 183e093e  .>.>$>.>'>.>.>.>
 13f30 0a3e0000 48656c6c 6f20576f 726c6421  .>..Hello World!
 13f40 00000000 78203d20 25642c20 79203d20  ....x = %d, y = 
 13f50 25642c20 7a203d20 25642c20 74203d20  %d, z = %d, t = 
 13f60 25640a00 48656c6c 6f20576f 726c6421  %d..Hello World!
 13f70 0a000000 f0430200 0a000000 494e4600  .....C......INF.
```

La section .rodata.str1.4 ne sert donc que pour fournir une information supplémentaire sur les chaines de caractères qui s'y trouvent au linker.

Quelques tests nous montrent rapidement que si deux fichiers objets contiennent 2 chaînes de caractères identiques dans la sections _.rodata.str1.4_ celles-ci sont fusionnés lors de l'édition des liens tandis que cela n'a jamais lieu pour _.rodata_

En désassemblant le main (`arm-none-eabi-objdump --disassemble=main sections1.elf`) on trouve :

```nasm
0000821c <main>:
...
    8240:   e59f0034    ldr r0, [pc, #52]   ; 827c <main+0x60>
    8244:   eb0000e2    bl  85d4 <puts>
...
    827c:   00013f34    .word   0x00013f34
```

En regardant à l'adresse 0x00013f34 dans _.rodata_ on retrouve bien que c'est le "Hello World" sans saut de ligne qui est utilisé par puts.

**Conclusion :** La chaîne est dupliquée pour conserver la chaîne initiale que l'utilisateur a voulu stocker dans mesg et pour avoir une chaîne adaptée à l'envoi à puts (sans le retour chariot). Avec -O1, la seconde chaîne est placée dans la section _.rodata.str1.4_ dont le but est de fusionner les chaînes de caractères destinées à l'affichage (le 1 de str1 indiquerait donc la sortie standard).

### Optimisation -Os
```
Sections:
Idx Name          Size    
  0 .text         00000000  
  1 .data         00000004  
  2 .bss          00000005  
  3 .rodata.str1.1 0000002d 
  4 .text.startup 00000068  
  5 .rodata       0000000e  
  6 .comment      0000004a  
  7 .ARM.attributes 0000002a
```

On remarque que la section _.text_ est vide. Tout le code a été transféré par gcc dans une section appelée _.text.startup_ dont l'objectif est d'accueillir le code qui est exécuté uniquement au démarrage du programme. La séparation de ce code permet de décharger de la mémoire le code qui ne sera plus exécuté après le début du programme (ici tout le code est "froid"). 

La section _.text.startup_ est d'ailleurs légèrement plus petite que la section _.text_ du programme compilé avec -O1 grâce aux optimisations du compilateurs (utilisation d'instructions multiples, etc.).

La section _.rodata.str1.4_ est devenu _.rodata.str1.1_ est est plus petite de 2 octets. Observons la différence entre les deux :

| .rodata.str1.4                                                | .rodata.str1.1                                                |
| ---                                                           | ---                                                           |
| `0000 48656c6c 6f20576f 726c6421 00000000  Hello World!....`<br>`0010 78203d20 25642c20 79203d20 25642c20  x = %d, y = %d,`<br>`0020 7a203d20 25642c20 74203d20 25640a00  z = %d, t = %d..`       | `0000 48656c6c 6f20576f 726c6421 0078203d  Hello World!.x =`<br>`0010 2025642c 2079203d 2025642c 207a203d   %d, y = %d, z =`<br>`0020 2025642c 2074203d 2025640a 00         %d, t = %d..` |
 
Les deux sections semblent ne pas avoir les mêmes **contraintes d'alignement**. Vérifions le avec la commande `arm-none-eabi-objdump -h` :

| .rodata.str1.4                                                    | .rodata.str1.1                                                    |
| ---                                                               | ---                                                               |
| `Idx Name          Size      VMA       LMA       File off  Algn`<br>`  3 .rodata.str1.4 00000030  00000000  00000000  000000a4  2**2 `<br>`                  CONTENTS, ALLOC, LOAD, READONLY, DATA` | `Idx Name          Size      VMA       LMA       File off  Algn`<br>`  3 .rodata.str1.1 0000002d  00000000  00000000  00000038  2**0`<br>`                  CONTENTS, ALLOC, LOAD, READONLY, DATA` |

On constate en effet que _.rodata.str1.4_ respecte un alignement de 2²=4 octets quand _.rodata.str1.1_ respecte une contrainte d'alignement de 1 octet (pas de contrainte). Ceci explique la différence de taille des deux sections.

### Optimisation -02
```
Sections:
Idx Name          Size    
  0 .text         00000000  
  1 .data         00000004  
  2 .bss          00000005  
  3 .rodata.str1.4 00000030 
  4 .text.startup 0000006c  
  5 .rodata       0000000e  
  6 .comment      0000004a  
  7 .ARM.attributes 0000002a
```

L'option -O2 est à mi-chemin entre -O1 et -Os : le code fait la même taille que pour -O1 mais est stocké dans _.text.startup_ et les sections _.rodata_ et _.rodata.str1.4_ sont identiques à celle de -O1.

## Question 2

Remplaçons `const char mesg[]` par `static const char mesg[]` dans notre fichier .c

Pour le niveau d'optimisation -O0, rien ne change. Mais pour les niveaux -O1 -O2 -Os, la section .rodata qui contenait la chaîne originale n'existe plus. En effet, le compilateur a constaté qu'elle n'était pas utilisée dans le fichier et comme elle est maintenant déclarée static elle ne peut de toute façon pas être utilisée par un autre fichier et n'a pas besoin d'être conservée.

## Question 3

Remplaçons `const char mesg[]` par `const char *mesg`.

Pour le niveau d'optimisation -O0, nous avons les nouvelles tailles de sections :

```
Sections:
Idx Name          Size    
  0 .text         000000c0  
  1 .data         00000008  
  2 .bss          00000005  
  3 .rodata       00000030  
  4 .comment      0000004a  
  5 .ARM.attributes 0000002a
```

La section _.text_ fait 0xc0 octets contre 0xb8 octets pour le fichier source initial. Examinons le code généré (`arm-none-eabi-objdump -S`):

```nasm
Disassembly of section .text:

00000000 <main>:
...
  58:   e59f3054    ldr r3, [pc, #84]   ; b4 <main+0xb4>
  5c:   e5933000    ldr r3, [r3]
  60:   e1a00003    mov r0, r3
  64:   ebfffffe    bl  0 <printf>
...
  90:   e59f0024    ldr r0, [pc, #36]   ; bc <main+0xbc>
  94:   ebfffffe    bl  0 <printf>
...
  b0:   00000004    .word   0x00000004
    ...
  bc:   00000010    .word   0x00000010
```

Ici printf n'est plus remplacé par puts pour le même niveau d'optimisation. En effet, pour optimiser cet appel, gcc a besoin de connaître la chaîne passée à printf, ce qui est bien le cas pour `const char mesg[]` mais plus pour `const char* mesg` qui n'est plus un pointeur constant. Un autre fichier pourrait donc modifier l'adresse stockée dans ce pointeur empêchant l'optimisation de l'instruction par gcc.

En l'absence d'optimisation la chaîne n'est plus dupliquée, ce qui justifie la diminution de taille de la section _.rodata_
```
Contents of section .rodata:
 0000 48656c6c 6f20576f 726c6421 0a000000  Hello World!....
 0010 78203d20 25642c20 79203d20 25642c20  x = %d, y = %d, 
 0020 7a203d20 25642c20 74203d20 25640a00  z = %d, t = %d..
```

Si maintenant l'on remplace `const char mesg[]` par `const char * const mesg` alors la section _.text_ fait la même taille que pour le fichier source initial et l'on retrouve l'optimisation de gcc pour printf :
```nasm
Disassembly of section .text:

00000000 <main>:
...
  58:   e59f004c    ldr r0, [pc, #76]   ; ac <main+0xac>
  5c:   ebfffffe    bl  0 <puts>
...
  a4:   00000000    .word   0x00000000
  a8:   00000004    .word   0x00000004
  ac:   00000014    .word   0x00000014
...
```

On retrouve aussi la duplication de la chaîne dans _.rodata_ :
```
Contents of section .rodata:
 0000 48656c6c 6f20576f 726c6421 0a000000  Hello World!....
 0010 00000000 48656c6c 6f20576f 726c6421  ....Hello World!
 0020 00000000 78203d20 25642c20 79203d20  ....x = %d, y = 
 0030 25642c20 7a203d20 25642c20 74203d20  %d, z = %d, t = 
 0040 25640a00                             %d..
```

**Remarque :** La chaîne (constante) est stockée dans _.rodata_ tandis que le pointeur mesg (constant) est stocké dans _.text_

On peut vérifier que le compilateur a le même comportement pour les autres niveaux d'optimisation.

# Exercice Makefile


```make
PREFIX=arm-none-eabi-
CC    = $(PREFIX)gcc
CFLAGS  = -Wall -Werror -g -Og
TARGET_ARCH = -mthumb
LDFLAGS = -L/opt/mylibs
LDLIBS = -lm

OBJS = main.o t1.o stubs.o t2.o
EXEC = hello

all: $(EXEC)

$(EXEC): $(OBJS)
    $(LINK.o) $^ $(LDLIBS) -o $@

stubs.o: libs/stubs.c
    $(CC) -g -O2 -mthumb -c -o $@ $<

clean::
    rm -f $(EXEC) $(OBJS)
```
